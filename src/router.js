import { createRouter, createWebHistory } from 'vue-router'
import Homepage from './components/Homepage'
import Movie from "./components/Movie";
import LoginDialog from "./components/LoginDialog";

const routes = [
    {
        path: '/',
        component: Homepage
    },

    {
        path: '/movie',
        component: Movie,
    },
    {
        path: '/login',
        component: LoginDialog,
    }
]
const router = createRouter({
    history: createWebHistory(),
    routes
})
export default router;