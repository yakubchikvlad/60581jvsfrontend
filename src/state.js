import {createStore} from "vuex";
import router from "./router";

const backendUrl = process.env.VUE_APP_BACKEND_URL;
const client_id = process.env.VUE_APP_CLIENT_ID;
const client_secret = process.env.VUE_APP_CLIENT_SECRET;

const store = createStore({
    state: {
        user: {},
        token: null,
        preLoading: false,
        dataPreLoading: true,
        loginError: false,
        loggedIn: false,
        movie: {},
        pager: {
            currentPage: 1,
            perPage: 5,
        },
        validation: {},
        createMovieDialogVisible: false,
    },
    mutations: {
        setToken(state, token) {
            state.token = token;
        },

        getToken(context, token) {
            context.token = token;
        },

        setUser(context, user) {
            context.user = user;
        },

        setPreloading(context, is_load) {
            context.preLoading = is_load;
        },
        setDataPreloading(context, is_load) {
            context.dataPreLoading = is_load;
        },
        setLoginError(context, isError) {
            context.loginError = isError
        },
        setLoggedIn(context, isLoggedIn) {
            context.loggedIn = isLoggedIn
        },
        setMovie(context, movie) {
            context.movie = movie
        },
        setPage(context, page) {
            context.pager.currentPage = page
        },
        setPager(context, pager) {
            context.pager = pager
        },
        setValidation(context, validation) {
            context.validation = validation;
        },
        setCreateMovieDialogVisible(context, visible) {
            context.createMovieDialogVisible = visible;
        },
        logout(context) {

            context.user = null;
            context.token = null;
            context.loggedIn = false;
            context.movie = {};
            localStorage.removeItem('token')
            router.push('/')
        },
    },
    actions: {
        auth(context, {login, password}) {
            console.log("auth start");
            context.commit('setPreloading', true)
            window.axios.post(backendUrl + '/OAuthController/Authorize', {
                username: login,
                password: password,
                grant_type: 'password'
            }, {
                headers: {
                    Authorization: 'Basic ' + window.btoa(client_id + ':' + client_secret),
                    'Access-Control-Allow-Origin':"*",
                    'Access-Control-Allow-Headers':'Origin, X-Requested-With, Content-Type, Accept',
                }
            }).then((response) => {
                console.log("we have respond");

                if(response.data.access_token){
                    context.commit('setToken', response.data.access_token)
                    localStorage.setItem('token', response.data.access_token);
                    context.commit('setLoggedIn', true);
                    context.commit('setLoginError', false);
                    context.dispatch('getUser');
                } else {
                    context.commit('setLoginError', true)
                    context.commit('setPreloading', false)
                }

            })

        },
        getUser(context) {
            //context.commit('setPreloading', true);
            console.log('get user')
            return window.axios.get(backendUrl + '/OAuthController/user', {
                headers: {
                    Authorization: 'Bearer ' + context.state.token,
                    'Access-Control-Allow-Origin':"*",
                    'Access-Control-Allow-Headers':'Origin, X-Requested-With, Content-Type, Accept',
                }
            }).then((response) => {

                console.log(response.data);
                //let str = ((response.data)+'').split('<')[0]//обрезаем некорректную часть, оставляя тока JSON
                //context.commit('setUser', JSON.parse(JSON.stringify(str)));

                context.commit('setUser', response.data);
                context.commit('setPreloading', false);

            }).catch(error => {
                    if (error.response) context.commit('setLoggedIn', false);
                }
            )

        },
        getMovie(context) {
            console.log('get movie')
            context.commit('setDataPreloading', true)
            const params = new URLSearchParams()
            params.append('per_page', context.state.pager.perPage)



            return window.axios.post(backendUrl + '/MovieApi/movie?page_group1=' + context.state.pager.currentPage, params,
                {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token,
                        'Access-Control-Allow-Origin':"*",
                        'Access-Control-Allow-Headers':'Origin, X-Requested-With, Content-Type, Accept, Options',
                    },
                }).then((response) => {


                context.commit('setMovie', response.data.movie);
                context.commit('setPager', response.data.pager);
                context.commit('setDataPreloading', false)
                console.log(response.data.movie)
                console.log(response.data.pager)

            })
        },

        createMovie(context, {name, lasting}) {
            console.log('create movie')
            context.commit('setDataPreloading', true)
            const params = new URLSearchParams()
            params.append('name', name)
            params.append('lasting', lasting)
            return window.axios.post(backendUrl + '/MovieApi/store', params,
                {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token
                    },
                }).then((response) => {
                if (response.status === 201) {
                    console.log("Movie created successfully");
                    context.commit("setValidation", {})
                    context.commit('setCreateMovieDialogVisible', false);
                    context.commit('setPage', context.state.pager.pageCount);
                    this.dispatch('getMovie');
                    router.push('/movie');
                } else {
                    console.log(response.data)
                    context.commit("setValidation", response.data)
                }
            })
        },

        deleteMovie(context, {id}) {
            console.log('delete movie')
            context.commit('setDataPreloading', true)
            return window.axios.get(backendUrl + '/MovieApi/delete/' + id,
                {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token
                    },
                }).then((response) => {
                if (response.status === 200) {
                    console.log("Movie deleted successfully");
                    context.commit('setPage', context.state.pager.pageCount);
                    this.dispatch('getMovie');
                    router.push('/movie');
                } else {
                    console.log(response.data)
                }
            })
        }
    }
})

export default store;